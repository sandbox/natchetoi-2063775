<?php

/*
 * @file fomatter data
 */
/*
 * implements hook_formatter_info()
 */
function jorgchart_field_field_formatter_info() {
    return array(
        // This formatter just displays the hex value in the color indicated.
        'jorgchart_default_formatter' => array(
            'label' => t('jOrgChart Default Formatter'),
            'field types' => array('jorgchart_field'),
        ),
    );
}

/*
 * implements hook_field_formatter_view()
 */
function jorgchart_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

    $element = array();

    switch ($display['type']) {
        // Override check plain
        case 'jorgchart_default_formatter':
            foreach ($items as $delta => $item) {
                $name = ($item['tid'] != 'autocreate' ? $item['taxonomy_term']->name : $item['name']);
                $element[$delta] = array(
                    '#markup' => mymodule_crazy_fun_plain($name),
                );
            }
            break;
    }
    return $element;
}