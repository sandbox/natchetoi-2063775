<?php

/**
 * @file 
 * widget data
 */

/**
 * implements hook_widget_info()
 */
function jorgchart_field_field_widget_info() {
    return array(
        'jorgchart_default_widget' => array(
            'label' => t('jOrgChart Default Widget'),
            'field types' => array('jorgchart_field'),
        ),
    );
}

/**
 * implements hook_widget_form()
*/
function jorgchart_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
    $value = isset($items[$delta]['jorgchart']) ? $items[$delta]['jorgchart'] : '';

    $widget = $element;
    $widget['#delta'] = $delta;

    switch ($instance['widget']['type']) {

        case 'jorgchart_default_widget':
            // Convert rgb value into r, g, and b for #default_value.

            $widget['create_jorgchart_format_type'] = array(
                '#type' => 'markup',
                '#markup'=>"<div class='test'><h1>test</h1></div>"
            );
            $widget['jorgchart'] = array(
                '#type'=>'jorgchart',
                '#title' => 'jorgchart field',
                '#default_value'=>'root',
            );
            if ($instance['required'] == 1)
                $widget[$key]['#required'] = 1;

            break;
    }

    $element['jorgchart'] = $widget;
    //drupal_set_message("<pre>".print_r($widget,true)."</pre");
    return $element;
}