<?php

/**
 * @file
 * Contains hook_views_plugins().
 */
/**
 * Implements hook_views_plugins().
 * @return type
 */
function jorgchart_views_views_plugins() {

  return array(
    'module' => 'jorgchart_views',
    'style' => array(
      'default_chart' => array(
        'title' => t('jOrgChart chart'),
        'handler' => 'jorgchart_views_default_chart',
        'path' => drupal_get_path('module', 'jorgchart_views') . '/plugins',
        //'theme' => 'views_yandexmaps_default_map',
        // Список js файлов, которые необходимо подключить при выводе представления.
        // Пути указываются относительно корня Drupal. Необязательно.
        'js' => array(),
        // Тип плагина.
        'type' => 'normal',
        // TRUE если для этого стиля можно выбирать row plugin-ы (Content, Fields и т.д.).
        'uses row plugin' => TRUE,
        // TRUE если можно использовать поля.
        'uses fields' => TRUE,
        // TRUE если у стиля есть настройки.
        'uses options' => TRUE,
        // TRUE если строки можно группировать.
        'uses grouping' => FALSE,
        // TRUE если нужно рендерить представление, даже при отсутствии данных.
        'even empty' => FALSE,
      ),
      'project_management_chart' => array(
        'title' => t('jOrgChart Project Management chart (not working yet)'),
        'help' => t('Displays data in jOrgChart view'),
        'handler' => 'jorgchart_views_plugin_style_default_chart',
        'path' => drupal_get_path('module', 'jorgchart_views') . '/plugins',
        'theme' => 'views_yandexmaps_default_map',
        'js' => array(),
        'type' => 'normal',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'even empty' => FALSE,
        'use ajax' => FALSE,
        'use relations' => FALSE,
      ),
    ),
  );
}