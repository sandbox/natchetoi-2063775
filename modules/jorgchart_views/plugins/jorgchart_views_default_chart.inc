<?php

/**
 * @file
 * Contains the list style plugin.
 */

/**
 * Style plugin to render jOrgChart tree.
 *
 * @ingroup views_style_plugins
 */
class jorgchart_views_default_chart extends views_plugin_style {

  /**
   * default settings
   */
  public function option_definition() {

    $options = parent::option_definition();
    $options['entity_id'] = array('default' => '');
    $options['entity_parent_id'] = array('default' => '');
    $options['block_title'] = array('default' => '');
    $options['block_body'] = array('default' => '');
    $options['jorgchart_options'] = array('default' => '');
    $options['jorgchart_skin'] = array('default' => '');
    return $options;
  }

  public function get_fields() {
    $field_names = array('' => t('<none>'));
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_names[$id] = $handler->ui_name(FALSE);
    }
    return $field_names;
  }

  /**
    settings form
   */
  public function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);




    $fields = array('' => t('<none>'));

    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      if ($label = $handler->label()) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }
    $form['entity_id'] = array(
      '#title' => t('Entity Id'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['entity_id'],
    );

    $form['entity_parent_id'] = array(
      '#title' => t('Entity parent Id'),
      '#description' => 'Select Entity Parent',
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['entity_parent_id'],
    );

    $form['block_title'] = array(
      '#title' => t('Block Title'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['block_title'],
    );

    $form['block_body'] = array(
      '#title' => t('Block body'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['block_body'],
    );

    $form['jorgchart_skin'] = array(
      '#title' => t('jOrgChart views style'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => unserialize(JORGCHART_VIEWS_STYLE),
      '#default_value' => $this->options['jorgchart_skin'],
    );

    $form['options'] = array(
      '#title' => t('Tools'),
      '#type' => 'checkboxes',
      '#options' => unserialize(JORGCHART_VIEWS_OPTIONS),
      '#default_value' => $this->options['jorgchart_options'],
    );
  }

  public function render() {

    if (isset($this->view->live_preview) && $this->view->live_preview) {
      return t('Selected style are not compatible with live preview.');
    }

    $this->render_fields($this->view->result);

    $tree = array();





    foreach ($this->rendered_fields as $val) {

      $tree_node = array(
        'entity_id' => $val[$this->options['entity_id']],
        'entity_parent_id' => $val[$this->options['entity_parent_id']],
        'entity_title' => $val[$this->options['block_title']]
      );


      if (empty($this->options['block_body'])) {
        foreach ($this->options['block_body'] as $entity_content_fields) {
          $tree_node['entity_content'][$entity_content_fields] = $val[$entity_content_fields];
        }
      }

      $tree[] = $tree_node;
    }

    $cats = jorgchart_views_prepare_list($tree);

    foreach ($this->view->result as $row_index => $row) {
      $marker_title = '';
    }

    $chart_id = drupal_html_id('jchart-views-views-' . $this->view->name . '-' . $this->view->current_display);

    $ul = jorgchart_views_buid_tree($cats, 0);

    $ul = '<ul id="' . $chart_id . '"><li><div>ROOT</div>' . $ul . '</li></ul>';

    jorgchart_add_library($this->options['jorgchart_skin']);

    drupal_add_js('jQuery(document).ready(function() {
					jQuery("#' . $chart_id . '").after("<div id=\'chart\' class=\'orgChart\'></div>");
					jQuery("#' . $chart_id . '").css("display","none");
					jQuery("#' . $chart_id . '").jOrgChart({
						chartElement : "#chart",
						dragAndDrop  : true
					});
				});
			', 'inline', array('scope' => 'footer'));
    //TODO write sequre code
    return $ul;

    return theme($this->theme_functions(), array(
      'view' => $this->view,
      'options' => $this->options,
      'attributes_array' => array(
        'style' => 'width:' . $this->options['map_width'] . '; height:' . $this->options['map_height'],
        'id' => $id,
        'class' => 'yandex_default_map',
      ),
    ));
  }

}
