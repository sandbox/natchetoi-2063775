<?php
/**
 * @file
 * Contains the list style plugin.
 */

/**
 * Style plugin to render each item in a slideshow of an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class jorgchart_views_plugin_style_taxonomy_chart extends views_plugin_style {

  /**
   * default settings
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['entity_id'] = array('default' => '');
    $options['entity_parent_id'] = array('default' => '');
    $options['block_title'] = array('default' => '');
    $options['block_body'] = array('default' => '');
    $options['jorgchart_options'] = array('default' => '');
    $options['jorgchart_views_style'] = array('default' => '');
    return $options;
  }

  function get_fields() {
    $field_names = array('' => t('<none>'));
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_names[$id] = $handler->ui_name(FALSE);
    }
    return $field_names;
  }

  /**
    settings form
   */
  function options_form(&$form, &$form_state) {

    parent::options_form($form, $form_state);




    $fields = array('' => t('<none>'));

    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      if ($label = $handler->label()) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }
    $form['entity_id'] = array(
      '#title' => t('Entity Id'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['entity_id'],
    );

    $form['entity_parent_id'] = array(
      '#title' => t('Entity parent Id'),
      '#description' => 'Select Entity Parent',
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['entity_parent_id'],
    );

    $form['block_title'] = array(
      '#title' => t('Block Title'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['block_title'],
    );

    $form['block_body'] = array(
      '#title' => t('Block body'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $fields,
      '#default_value' => $this->options['block_body'],
    );

    $form['jorgchart_views_style'] = array(
      '#title' => t('jOrgChart views style'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => unserialize(JORGCHART_VIEWS_STYLE),
      '#default_value' => $this->options['entity_id'],
    );
    $form['options'] = array(
      '#title' => t('Tools'),
      '#type' => 'checkboxes',
      '#options' => unserialize(JORGCHART_VIEWS_OPTIONS),
      '#default_value' => $this->options['jorgchart_options'],
    );
  }

  function render() {

    if (isset($this->view->live_preview) && $this->view->live_preview) {
      return t('Selected style are not compatible with live preview.');
    }

    $this->render_fields($this->view->result);

    $tree = array();



    foreach ($this->rendered_fields as $val) {

      $tree_node = array(
        'entity_id' => $val[$this->options['entity_id']],
        'entity_parent_id' => $val[$this->options['entity_parent_id']],
        'entity_title' => $val[$this->options['block_title']]
      );

      foreach ($this->options['block_body'] as $entity_content_fields) {
        $tree_node['entity_content'][$entity_content_fields] = $val[$entity_content_fields];
      }
      $tree[] = $tree_node;
    }

    //$voc=taxonomy_vocabulary_load(2);
    //drupal_set_message("<pre>".print_r($tree,true)."</pre>");
    $cats = jorgchart_views_prepare_list($tree);

    /*
      Почему-то в пермо случае первый элемент имеет номер 1
      а во втором случае номр 0
      исправить
     */
    //$tree=taxonomy_get_tree(2,0,NULL,TRUE);	
    //$cats	=	jorgchart_views_prepare_list($tree);


    foreach ($this->view->result as $row_index => $row) {




      $marker_title = '';
    }

    $chart_id = drupal_html_id('jchart-views-views-' . $this->view->name . '-' . $this->view->current_display);

    $ul = jorgchart_views_buid_tree($cats, 0);

    $ul = '<ul id="' . $chart_id . '"><li><div>Superparent</div>' . $ul . '</li></ul>';

    drupal_add_js('jQuery(document).ready(function() {
					jQuery("#' . $chart_id . '").after("<div id=\'chart\' class=\'orgChart\'></div>");
					jQuery("#' . $chart_id . '").css("display","none");
					
					jQuery("#' . $chart_id . '").jOrgChart({
						chartElement : "#chart",
						dragAndDrop  : true
					});
				});
			', 'inline', array('scope' => 'footer'));
    return $ul;



    return theme($this->theme_functions(), array(
      'view' => $this->view,
      'options' => $this->options,
      'attributes_array' => array(
        'style' => 'width:' . $this->options['map_width'] . '; height:' . $this->options['map_height'],
        'id' => $id,
        'class' => 'yandex_default_map',
      ),
    ));
  }

}
